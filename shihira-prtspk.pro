CONFIG += qt
QT += widgets gui core xml multimedia multimediawidgets

SOURCES += prtspk.cc
HEADERS += prtspk.h
FORMS += prtspk.ui
RESOURCES += resource.qrc

QTPLUGIN += dsengine qtmedia_audioengine qwindows
RC_FILE += resource.rc

