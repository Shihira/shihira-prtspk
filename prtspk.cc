#include <QApplication>
#include <QtPlugin>
#include "prtspk.h"

//Q_IMPORT_PLUGIN(dsengine)
//Q_IMPORT_PLUGIN(qtmedia_audioengine)
//Q_IMPORT_PLUGIN(qwindows)

void prtspk::set_progress(qint64 pos) {
        qreal rpos = pos / 1000;
        qreal rdur = media->duration() / 1000;
        progress->setValue(rpos / rdur * 100);
        progress->setFormat(QString("%1 s left " + cur_play_hint).arg(int(rdur - rpos)));
}

void prtspk::stop_slot(QMediaPlayer::State state) {
        if(state == QMediaPlayer::StoppedState)
                next_media();
}

void prtspk::reset_all() {
        media->stop();
        delayer_counter = 0;
}

void prtspk::delayer() {
        progress->setValue((delayer_total - delayer_counter) * 100 / delayer_total);
        progress->setFormat(QString("%1 s left " + cur_play_hint).arg(delayer_counter));

        if(--delayer_counter >= 0)
                QTimer::singleShot(1000, this, SLOT(delayer()));
        else emit delayed();
}

void prtspk::open_dir() {
        QString dir = QFileDialog::getExistingDirectory();
        activateWindow();
        QDir::setCurrent(dir);

        load_xml();
        //for(int i=0; i<9; i++)
        next_media();
}

void prtspk::next_media() {
        if(infor_row->isNull()) return;
        cur_file_path = infor_row->attribute("FilePath");
        cur_play_hint = infor_row->attribute("PlayHint");
        cur_file_type = infor_row->attribute("FileType").toInt();
        cur_play_time = infor_row->attribute("PlayTime").toInt();
        cur_play_flag = infor_row->attribute("PlayFlag").toInt();

        qDebug() << cur_file_path << cur_file_type << cur_play_flag << cur_play_time << cur_play_hint;

        //reset all previous connections
        disconnect(media, SIGNAL(stateChanged(QMediaPlayer::State)),
                this, SLOT(stop_slot(QMediaPlayer::State)));
        disconnect(this, SIGNAL(delayed()),
                this, SLOT(next_media()));
        if(recorder->status() == QMediaRecorder::RecordingStatus)
                recorder->stop();

        //determine file type first, and launch the display controllers
        switch(cur_file_type) {
        case FILE_TYPE_TEXT: {
                video->hide(); fakevideo->show();
                QFile ftxt(cur_file_path); ftxt.open(QIODevice::ReadOnly);
                fakevideo->setText(QString::fromLocal8Bit(ftxt.readAll()));
                ftxt.close();
                break;
        }
        case FILE_TYPE_MOVIE:
                video->show(); fakevideo->hide();
        case FILE_TYPE_SOUND:
                media->setMedia(QUrl::fromLocalFile(cur_file_path));
                media->play();
                break;
        case FILE_TYPE_DELAY:
                break;
        case FILE_TYPE_SAVE:
                QDir cwd = QDir::current();
                if(!cwd.exists("Answer")) cwd.mkdir("Answer");

                recorder->setOutputLocation(QUrl::fromLocalFile(cwd.absolutePath() + 
                        QString("/Answer/") + cur_file_path));
                recorder->record();
                break;
        }

        //set up stop connections according to the xmlinfo
        switch(cur_play_time) {
        case PLAY_TIME_DECIDED_BY_DURATION:
                connect(media, SIGNAL(stateChanged(QMediaPlayer::State)),
                        this, SLOT(stop_slot(QMediaPlayer::State)));
                break;
        case PLAY_TIME_DECIDED_BY_NEXT:
                // jump to next
                (*infor_row) = infor_row->nextSiblingElement();
                next_media();
                return;
        default:
                if(cur_file_type == FILE_TYPE_DELAY ||
                   cur_file_type == FILE_TYPE_SAVE) {
                        delayer_counter = cur_play_time;
                        delayer_total = cur_play_time;
                        QTimer::singleShot(1000, this, SLOT(delayer()));
                        connect(this, SIGNAL(delayed()),
                                this, SLOT(next_media()));
                }
        }

        //this is used only in the recording section of part A
        if(cur_file_type == FILE_TYPE_MOVIE ||
           cur_file_type == FILE_TYPE_SOUND)
        switch(cur_play_flag) {
        case PLAY_FLAG_MUTE:
                media->setVolume(0);
                // jump to next
                (*infor_row) = infor_row->nextSiblingElement();
                next_media();
                return;
        case PLAY_FLAG_NORMAL:
                media->setVolume(100); break;
        }

        (*infor_row) = infor_row->nextSiblingElement();
}

void prtspk::load_xml() {
        QFile file_infor("Information.xml");
        file_infor.open(QIODevice::ReadOnly);
        QString decoded = QString::fromLocal8Bit(file_infor.readAll());

        infor->setContent(decoded, false);
        infor_row = new QDomElement(infor->
                firstChildElement("DATAPACKET").
                firstChildElement("ROWDATA").
                firstChildElement("ROW"));
}

prtspk::prtspk() {
        setupUi(this);

        QTextCodec::setCodecForLocale(QTextCodec::codecForName("gbk"));

        recorder = new QAudioRecorder(this);

        //Settings
        QAudioEncoderSettings settings;
        settings.setCodec("audio/pcm");
        settings.setSampleRate(44100);
        settings.setBitRate(128000);
        settings.setChannelCount(2);
        settings.setEncodingMode(QMultimedia::ConstantBitRateEncoding);
        recorder->setEncodingSettings(settings, QVideoEncoderSettings(), "audio/x-wav");

        media = new QMediaPlayer();
        video = new QVideoWidget(this);
        infor = new QDomDocument();
        delayer_total = 0;
        delayer_counter = 0;

        layout_video->addWidget(video);
        media->setVideoOutput(video);

        connect(play, SIGNAL(clicked()), this, SLOT(open_dir()));
        connect(stop, SIGNAL(clicked()), this, SLOT(reset_all()));
        connect(media, SIGNAL(positionChanged(qint64)), this, SLOT(set_progress(qint64)));
}

int main(int argc, char* argv[])
{
        QApplication app(argc, argv);
        prtspk main_form;

        main_form.showFullScreen();
        app.exec();

        return 0;
}
