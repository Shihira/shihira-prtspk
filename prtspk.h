#include <QtMultimedia>
#include <QtMultimediaWidgets>
#include <QtXml>

#define FILE_TYPE_SAVE 0
#define FILE_TYPE_TEXT 1
#define FILE_TYPE_SOUND 3
#define FILE_TYPE_MOVIE 4
#define FILE_TYPE_DELAY 5

#define PLAY_TIME_DECIDED_BY_NEXT 0
#define PLAY_TIME_DECIDED_BY_DURATION -1

#define PLAY_FLAG_NORMAL 0
#define PLAY_FLAG_MUTE 1

#include "ui_prtspk.h"

//from pcm2wav.c
extern "C" int convert(const char* in, const char* out);

class prtspk : public QDialog, public Ui_main_form {
        Q_OBJECT

private:
        int delayer_counter;
        int delayer_total;

        QMediaPlayer* media;
        QVideoWidget* video;

        QDomDocument* infor;
        QDomElement* infor_row;

        QString cur_file_path;
        QString cur_play_hint;
        int cur_file_type;
        int cur_play_time;
        int cur_play_flag;

        void load_xml();

        QAudioRecorder* recorder;

signals:
        void delayed();

private slots:
        void reset_all();
        void delayer();
        void set_progress(qint64 pos);
        void stop_slot(QMediaPlayer::State state);

public slots:
        void next_media();
        void open_dir();

public:
        prtspk();
};
